<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class RoleTest extends TestCase
{
    public function testGetterAndSetters()
    {
        $role = new Role();

        $role->setName('Super Admin');

        $privileges = ["user_can_change_password", "role_can_delete_role"];
        $role->setPrivileges($privileges);

        $this->assertEquals($privileges, $role->getPrivileges());
        $this->assertEquals('Super Admin', $role->getName());

        $role->setCustom(true);
        $this->assertTrue($role->isCustom());

        $role->setCustom(false);
        $this->assertFalse($role->isCustom());
    }

    public function testAddRemovePrivilege()
    {
        $role = new Role();
        $privileges = ['first privilege', 'second privilege'];

        $role->setPrivileges($privileges);
        $this->assertEquals($privileges, $role->getPrivileges());

        $role->addPrivilege('additional privilege');
        $this->assertContains('additional privilege', $role->getPrivileges());
        $this->assertCount(3, $role->getPrivileges());

        $role->addPrivilege('next privilege');
        $this->assertContains('next privilege', $role->getPrivileges());
        $this->assertCount(4, $role->getPrivileges());

        $role->removePrivilege('next privilege');
        $this->assertNotContains('next privilege', $role->getPrivileges());
        $this->assertCount(3, $role->getPrivileges());

        $role->removePrivilege('additional privilege');
        $this->assertNotContains('additional privilege', $role->getPrivileges());
        $this->assertCount(2, $role->getPrivileges());
    }
}