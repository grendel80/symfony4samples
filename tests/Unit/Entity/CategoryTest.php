<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CategoryTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $category = new Category();
        $category->setName('Category');

        $subCategory1 = new Category();
        $subCategory1->setName('Subcategory 1');

        $subCategory2 = new Category();
        $subCategory2->setName('Subcategory 2');

        $category->addChildren($subCategory1)->addChildren($subCategory2);

        $this->assertEquals('Category', $category->getName());
        $this->assertInstanceOf(Category::class, $category);

        $this->assertEquals('Subcategory 1', $subCategory1->getName());
        $this->assertInstanceOf(Category::class, $subCategory1);

        $this->assertEquals('Subcategory 2', $subCategory2->getName());
        $this->assertInstanceOf(Category::class, $subCategory2);

        $this->assertEquals(2, $category->getChildren()->count());
    }



    public function testSetGetParent()
    {
        $category = new Category();

        $parentCategory = new Category();
        $parentCategory->setName('Parent Category');
        $category->setParent($parentCategory);

        $this->assertNotNull($category->getParent());
        $this->assertEquals('Parent Category', $category->getParent()->getName());
    }

    public function testSetGetChildren()
    {
        $parentCategory = new Category();
        $category1 = new Category();
        $category1->setName('subcategory 1');
        $category2 = new Category();
        $category2->setName('subcategory 2');
        $category3 = new Category();
        $category3->setName('subcategory 3');

        $parentCategory->setChildren(new ArrayCollection([$category1, $category2, $category3]));

        $this->assertCount(3, $parentCategory->getChildren());
        $this->assertEquals('subcategory 1', $parentCategory->getChildren()[0]->getName());
        $this->assertEquals('subcategory 2', $parentCategory->getChildren()[1]->getName());
        $this->assertEquals('subcategory 3', $parentCategory->getChildren()[2]->getName());

    }

    public function testRemoveChildren()
    {
        $category = new Category();
        $childCategory = new Category();

        $childCategory2 = new Category();
        $childCategory2->setName('First child category');

        $category->addChildren($childCategory)->addChildren($childCategory2);

        $category->removeChildren($childCategory);
        $this->assertEquals(1, $category->getChildren()->count());
        $this->assertEquals('First child category', $category->getChildren()[1]->getName());

        $category->removeChildren($childCategory2);
        $this->assertEquals(0, $category->getChildren()->count());
    }
}
