<?php

namespace App\Tests\Functional\Controller\Role;

use App\DataFixtures\Role\RoleAdminFixtures;
use App\DataFixtures\Role\RoleSuperAdminFixtures;
use App\DataFixtures\Role\RoleUserAdminFixtures;
use App\Entity\Role;
use App\Tests\Functional\ApiTestCase;
use App\Enum\Privileges\RolePrivileges;
use App\Enum\Privileges\UserPrivileges;
use Ramsey\Uuid\Uuid;

class RoleControllerTest extends ApiTestCase
{

    public function testListRequest()
    {
        $this->loadFixtures([RoleSuperAdminFixtures::class, RoleAdminFixtures::class]);

        $response = $this->request('GET', '/role');

        $this->assertJsonResponse();
        $this->assertEquals(200, self::$response->getStatusCode());
        $this->assertCount(2, $response);
    }

    public function testShowRequest()
    {
        $this->loadFixtures([RoleUserAdminFixtures::class]);

        $role = $this->findFirst(Role::class);

        $response = $this->request('GET', '/role/'.$role->getId());

        $this->assertJsonResponse();
        $this->assertEquals($role->getId(), $response->id);
        $this->assertEquals($role->getName(), $response->name);
        $this->assertEquals($role->getPrivileges(), $response->privileges);
    }

    public function testCreateCustomRoleWithProperData()
    {
        $body = ['name' => 'Admin',
            'privileges' =>
                [
                    RolePrivileges::ROLE_CAN_ADD_ROLE,
                    RolePrivileges::ROLE_CAN_DELETE_ROLE,
                    UserPrivileges::USER_CAN_ADD_USER,
                    UserPrivileges::USER_CAN_CHANGE_EMAIL,
                ],
            'custom' => 'true'
        ];

        $response = $this->request('POST', '/role', json_encode($body));

        $this->assertNotEmpty($response->id);
        $this->assertTrue($response->custom);
        $this->assertObjectNotHasAttribute('errors', $response);
    }

    public function testCreateNonCustomRoleWithProperData()
    {
        $body = ['name' => 'Admin',
            'privileges' =>
                [
                    RolePrivileges::ROLE_CAN_ADD_ROLE,
                    RolePrivileges::ROLE_CAN_DELETE_ROLE,
                    UserPrivileges::USER_CAN_ADD_USER,
                    UserPrivileges::USER_CAN_CHANGE_EMAIL,
                ]
        ];

        $response = $this->request('POST', '/role', json_encode($body));

        $this->assertNotEmpty($response->id);
        $this->assertFalse($response->custom);
        $this->assertObjectNotHasAttribute('errors', $response);
    }

    public function testCreateRoleFailsWithWrongDataNameToShort()
    {
        $body = ['name' => 'aa',
            'privileges' =>
                [
                    RolePrivileges::ROLE_CAN_ADD_ROLE,
                    RolePrivileges::ROLE_CAN_DELETE_ROLE,
                ],
            'custom' => true
        ];

        $response = $this->request('POST', '/role', json_encode($body));

        $this->assertEquals(400, self::$response->getStatusCode());
        $this->assertEquals('error.length.min', $response->name);
    }

    public function testCreateRoleFailsWithWrongDataWrongPrivileges()
    {
        $body = ['name' => 'Admin',
            'privileges' =>
                [
                    'some_non_existing_privilege',
                    'another_random_privilege',
                ],
            'custom' => true
        ];
        $response = $this->request('POST', '/role', json_encode($body));
        $this->assertEquals(400, self::$response->getStatusCode());
        $this->assertEquals('This value is not valid.', $response->privileges);
    }

    public function testUpdateRoleWithProperData()
    {
        $this->loadFixtures([RoleAdminFixtures::class]);
        $role = $this->getReference('role_admin');

        /** @var Role $role */
        $id=$role->getId();

        $body = ['name' => 'Admin updated',
            'privileges' =>
                [
                    RolePrivileges::ROLE_CAN_ADD_ROLE,
                    RolePrivileges::ROLE_CAN_DELETE_ROLE,
                ],
            'custom' => true
        ];

        $response = $this->request('PUT', 'role/'.$id, json_encode($body));
        $this->assertEquals(200, self::$response->getStatusCode());

        $this->assertEquals([
            RolePrivileges::ROLE_CAN_ADD_ROLE,
            RolePrivileges::ROLE_CAN_DELETE_ROLE
        ], (array)$response->privileges);

        $this->assertEquals('Admin updated', $response->name);
    }

    public function testTryUpdateNonExistingRole()
    {
        $uuid = Uuid::uuid4()->toString();
        $this->request('PUT', '/role/'.$uuid);

        $this->assertEquals(404, self::$response->getStatusCode());
    }

    public function testUpdateRoleWithWrongDataWrongPrivileges()
    {
        $this->loadFixtures([RoleAdminFixtures::class]);

        /** @var Role $role */
        $role = $this->findFirst(Role::class);

        $body = ['name' => 'aa',
            'privileges' =>
                [
                    'some_non_existing_privilege',
                    'another_random_privilege',
                ],
            'custom' => true
        ];

        $this->request('PUT', '/role/'.$role->getId(), json_encode($body));
        $this->assertEquals(400, self::$response->getStatusCode());
        $this->assertContains('error.length.min', self::$response->getContent());
        $this->assertContains('This value is not valid.', self::$response->getContent());
    }

    public function testDeleteRole()
    {
        $this->loadFixtures([RoleSuperAdminFixtures::class]);

        $role = $this->findFirst(Role::class);

        $this->request('DELETE', '/role/'.$role->getId());
        $this->assertEquals(204, self::$response->getStatusCode());
        $this->assertFalse($this->em()->contains($role));
    }
}