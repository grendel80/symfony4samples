<?php

namespace App\Tests\Functional\Controller\Role;

use App\Tests\Functional\ApiTestCase;

class PrivilegesControllerTest extends ApiTestCase
{

    public function testListRequestWithoutParameter()
    {
        $response = $this->request('GET', '/privilege');

        $this->assertObjectNotHasAttribute('RolePrivileges', $response);
        $this->assertObjectNotHasAttribute('UserPrivileges', $response);

        $this->assertObjectHasAttribute('ROLE_CAN_ADD_ROLE', $response);
        $this->assertEquals('role_can_add_role', $response->ROLE_CAN_ADD_ROLE);

        $this->assertObjectHasAttribute('ROLE_CAN_DELETE_ROLE', $response);
        $this->assertEquals('role_can_delete_role', $response->ROLE_CAN_DELETE_ROLE);

        $this->assertObjectHasAttribute('USER_CAN_VIEW_USERS_LIST', $response);
        $this->assertEquals('user_can_view_users_list', $response->USER_CAN_VIEW_USERS_LIST);

        $this->assertObjectHasAttribute('USER_CAN_ADD_USER', $response);
        $this->assertEquals('user_can_add_user', $response->USER_CAN_ADD_USER);
    }

    public function testListRequestWithNestedParameterEqualsFalse()
    {
        $response = $this->request('GET', '/privilege?nested=false');

        $this->assertObjectNotHasAttribute('RolePrivileges', $response);
        $this->assertObjectNotHasAttribute('UserPrivileges', $response);

        $this->assertObjectHasAttribute('ROLE_CAN_ADD_ROLE', $response);
        $this->assertEquals('role_can_add_role', $response->ROLE_CAN_ADD_ROLE);

        $this->assertObjectHasAttribute('ROLE_CAN_DELETE_ROLE', $response);
        $this->assertEquals('role_can_delete_role', $response->ROLE_CAN_DELETE_ROLE);

        $this->assertObjectHasAttribute('USER_CAN_VIEW_USERS_LIST', $response);
        $this->assertEquals('user_can_view_users_list', $response->USER_CAN_VIEW_USERS_LIST);

        $this->assertObjectHasAttribute('USER_CAN_ADD_USER', $response);
        $this->assertEquals('user_can_add_user', $response->USER_CAN_ADD_USER);
    }

    public function testListRequestWithNestedParameterEqualsTrue()
    {
        $response = $this->request('GET', '/privilege?nested=true');

        $this->assertObjectHasAttribute('RolePrivileges', $response);
        $this->assertObjectHasAttribute('UserPrivileges', $response);

        $this->assertObjectHasAttribute('ROLE_CAN_ADD_ROLE', $response->RolePrivileges);
        $this->assertEquals('role_can_add_role', $response->RolePrivileges->ROLE_CAN_ADD_ROLE);


        $this->assertObjectHasAttribute('ROLE_CAN_DELETE_ROLE', $response->RolePrivileges);
        $this->assertEquals('role_can_delete_role', $response->RolePrivileges->ROLE_CAN_DELETE_ROLE);

        $this->assertObjectHasAttribute('USER_CAN_VIEW_USERS_LIST', $response->UserPrivileges);
        $this->assertEquals('user_can_view_users_list', $response->UserPrivileges->USER_CAN_VIEW_USERS_LIST);

        $this->assertObjectHasAttribute('USER_CAN_ADD_USER', $response->UserPrivileges);
        $this->assertEquals('user_can_add_user', $response->UserPrivileges->USER_CAN_ADD_USER);
    }
}