<?php

namespace App\Tests\Functional\Controller\Category;

use App\DataFixtures\Category\CategoryParentFixtures;
use App\DataFixtures\Category\CategoryWithThreeSub;
use App\Entity\Category;
use App\Tests\Functional\ApiTestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class CategoryControllerTest extends ApiTestCase
{

    public function testAddNewMainCategory()
    {
        $body = ['name' => 'Example main category'];

        $response = $this->request('POST', '/category', json_encode($body));

        $this->assertJsonResponse(self::$response, 200);
        $this->assertObjectHasAttribute('id',$response);

        /** @var Category $category */
        $category = $this->getRepository(Category::class)->find($response->id);


        $this->assertNull($category->getParent());
        $this->assertCount(0,$category->getChildren());
        $this->assertFalse($category->isDeleted());

        $this->assertEquals(
            $category->getUpdatedAt()->format('Y-m-d H:i:v'),
            $category->getCreatedAt()->format('Y-m-d H:i:v')
        );
    }

    public function testAddNewSubCategoryWithProperData()
    {
        $this->loadFixtures([CategoryParentFixtures::class], true);

        $parentCategory = $this->getReference('category_parent');

        $body = ['name' => 'Example category',
                'parent' => $parentCategory->getId(),
                ];

        $response = $this->request('POST', '/category', json_encode($body));
        $this->assertJsonResponse(self::$response, 200);
        $this->assertEquals($response->parent->id, $parentCategory->getId());

        /** @var Category $category */
        $category = $this->getRepository(Category::class)->find($response->id);

        $this->assertEquals($category->getName(), 'Example category');
        $this->assertCount(1,$parentCategory->getChildren());
    }

    public function testAddNewCategoryToCategoryWithParentReturnsValidationError()
    {
        $this->loadFixtures([CategoryWithThreeSub::class], false);

        $body = ['name' => 'Subcategory category',
            'parent' => $this->getReference('category_parent_with_children')->getChildren()[1]->getId(),
        ];

        $response = $this->request('POST', '/category', json_encode($body));
        $this->assertObjectHasAttribute('parent', $response);
        $this->assertEquals($response->parent, 'error.choice.category');
    }

    public function testAddCategoryNameToShort()
    {
        $body = ['name' => 'a'];
        $response = $this->request('POST', '/category', json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('name', $response);
        $this->assertEquals($response->name, 'error.length.min');
    }

    public function testAddCategoryNameToLong()
    {
        $body = ['name' => '123456789 123456789 123456789 123456789 123456789 1'];

        $response = $this->request('POST', '/category', json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('name', $response);
        $this->assertEquals($response->name, 'error.length.max');
    }

    public function testListCategories()
    {
        $this->loadFixtures([CategoryWithThreeSub::class]);

        $response = $this->request('GET', '/category');
        $this->assertJsonResponse(self::$response, 200);
        $this->assertCount(4,$response);
        $this->assertCount(3,$response[0]->children);

        $category = $this->getReference('category_parent_with_children');

        $this->assertEquals($response[0]->children[0]->name, $category->getChildren()[0]->getName());
        $this->assertCount(0, $response[0]->children[0]->children);

        $this->assertEquals($response[0]->children[1]->name, $category->getChildren()[1]->getName());
        $this->assertCount(0, $response[0]->children[1]->children);

        $this->assertEquals($response[0]->children[2]->name, $category->getChildren()[2]->getName());
        $this->assertCount(0, $response[0]->children[2]->children);
    }

    public function testShowCategoryWithSubcategories()
    {
        $this->loadFixtures([CategoryWithThreeSub::class]);
        $category = $this->getReference('category_parent_with_children');

        $response = $this->request('GET', '/category/'.$category->getId());
        $this->assertJsonResponse(self::$response, 200);

        $this->assertEquals($response->name, $category->getName());
        $this->assertCount(3, $response->children);
        $this->assertNull($response->parent);
        $this->assertNull($response->deletedAt);
        $this->assertFalse($response->deleted);

        $this->assertEquals($response->children[0]->name, $category->getChildren()[0]->getName());
        $this->assertCount(0, $response->children[0]->children);
        $this->assertEquals($response->id, $category->getChildren()[0]->getParent()->getId());

        $this->assertEquals($response->children[1]->name,  $category->getChildren()[1]->getName());
        $this->assertCount(0, $response->children[1]->children);
        $this->assertEquals($response->id, $category->getChildren()[1]->getParent()->getId());

        $this->assertEquals($response->children[2]->name, $category->getChildren()[2]->getName());
        $this->assertCount(0, $response->children[2]->children);
        $this->assertEquals($response->id, $category->getChildren()[2]->getParent()->getId());
    }

    public function testShowCategoryWithoutSubcategories()
    {
        $this->loadFixtures([CategoryParentFixtures::class]);
        $category = $this->getReference('category_parent');

        $response = $this->request('GET', '/category/'.$category->getId());

        $this->assertJsonResponse(self::$response, 200);
        $this->assertEquals($response->name, $category->getName());
        $this->assertCount(0, $response->children);
        $this->assertNull($response->parent);
        $this->assertFalse($response->deleted);
        $this->assertNull($response->deletedAt);
    }

    public function testTryToShowNonExistingCategory()
    {
        $uuid = Uuid::uuid4()->toString();
        $this->request('GET', '/category/'.$uuid);

        $this->assertEquals(404, self::$response->getStatusCode());
    }

    public function testUpdateMainCategoryNameWithProperData()
    {
        $this->loadFixtures([CategoryParentFixtures::class]);

        $category = $this->getReference('category_parent');

        $body = ['name' => 'Updated name'];

        $response = $this->request('PUT', '/category/'.$category->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 200);
        $this->assertObjectHasAttribute('name', $response);
        $this->assertEquals($category->getName(),'Updated name');
    }

    public function testUpdateMainCategoryNameWithWrongData()
    {
        $this->loadFixtures([CategoryParentFixtures::class]);

        $category = $this->getReference('category_parent');

        $body = ['name' =>'a'];

        $response = $this->request('PUT', '/category/'.$category->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('name', $response);
        $this->assertEquals($response->name, 'error.length.min');

        $body = ['name' => '123456789 123456789 123456789 123456789 123456789 1'];

        $response = $this->request('PUT', '/category/'.$category->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('name', $response);
        $this->assertEquals($response->name, 'error.length.max');

        // set parent to non existing category
        $body = ['name' =>'Some name', 'parent' => '2c631488-769a-48a5-af43-e04f7ba407c8'];

        $response = $this->request('PUT', '/category/'.$category->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('parent', $response);
        $this->assertEquals($response->parent, 'error.choice.category');
    }

    public function testTryToUpdateNonExistingCategory()
    {
        $uuid = Uuid::uuid4()->toString();
        $this->request('PUT', '/category/'.$uuid);

        $this->assertEquals(404, self::$response->getStatusCode());
    }

    public function testUpdateTryToSetRootCategoryAsChild()
    {
        $this->loadFixtures([CategoryParentFixtures::class, CategoryWithThreeSub::class], true);
        $rootCategory = $this->getReference('category_parent');
        $category = $this->getReference('category_parent_with_children');

        $body = ['name' =>'Moved to be a child', 'parent' => $category->getId()];

        $response = $this->request('PUT', '/category/'.$rootCategory->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 200);
        $this->assertCount(4,$category->getChildren());
        $this->assertEquals($category->getChildren()[3]->getName(), 'Moved to be a child');

    }

    public function testUpdateTryToAssignRootCategoryToChildThrowsError()
    {
        $this->loadFixtures([CategoryParentFixtures::class, CategoryWithThreeSub::class]);
        $rootCategory = $this->getReference('category_parent');
        $category = $this->getReference('category_parent_with_children');

        $body = ['name' =>'Name', 'parent' =>$category->getChildren()[0]];

        $response = $this->request('PUT', '/category/'.$rootCategory->getId(), json_encode($body));

        $this->assertJsonResponse(self::$response, 400);
        $this->assertObjectHasAttribute('parent', $response);
        $this->assertEquals($response->parent, 'error.choice.category');
    }

    public function testTryDeleteNonExistingCategory()
    {
        $uuid = Uuid::uuid4()->toString();
        $this->request('DELETE', '/category/'.$uuid);

        $this->assertEquals(404, self::$response->getStatusCode());
    }

    public function testTryToDeleteCategoryWithChildren()
    {
        $this->loadFixtures([CategoryWithThreeSub::class]);
        $category = $this->getReference('category_parent_with_children');

        $response = $this->request('DELETE', '/category/'.$category->getId());
        $this->assertJsonResponse(self::$response, 500);
        $this->assertObjectHasAttribute('error', $response);
        $this->assertObjectHasAttribute('message', $response->error);
        $this->assertEquals($response->error->message, 'error.category.has.children');
    }

    public function testDeleteCategoryWithoutChildren()
    {
        $this->loadFixtures([CategoryParentFixtures::class]);
        $category = $this->getReference('category_parent');

        $response = $this->request('DELETE', '/category/'.$category->getId());

        $this->assertJsonResponse(self::$response, Response::HTTP_NO_CONTENT);
        $this->assertTrue($category->isDeleted());
    }

    public function testDeleteSubCategory()
    {
        $this->loadFixtures([CategoryWithThreeSub::class]);
        $category = $this->getReference('category_parent_with_children');

        $response = $this->request('DELETE', '/category/'.$category->getChildren()[0]->getId());

        $this->assertJsonResponse(self::$response, Response::HTTP_NO_CONTENT);
        $this->assertFalse($category->isDeleted());
        $this->assertFalse($category->getChildren()[1]->isDeleted());
        $this->assertFalse($category->getChildren()[2]->isDeleted());
        $this->assertCount(2, $category->getChildren());
    }
}
