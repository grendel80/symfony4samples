<?php declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Finder\Finder;

class PrivilegesService
{
    /**
     * @param bool $nested
     * @return array
     */
    public static function list($nested = false): array
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../Enum/Privileges');
        $priviligesArray = [];

        foreach ($finder as $file) {
            $className = str_replace('.php', '', $file->getFilename());
            $classNameWithPath = 'App\\Enum\\Privileges\\'.$className;
            $priviligesArray[$className] = $classNameWithPath::toArray();
        }

        $priviligesList = $priviligesArray;

        if (!$nested) {
            $priviligesList = array_merge(...array_values($priviligesArray));
        }

        return $priviligesList;
    }
}