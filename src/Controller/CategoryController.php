<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Manager\Category\CategoryManager;
use App\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends ApiController
{
    /**
     * @Route("/category", name="api_category_list")
     * @Method("GET")
     * @param CategoryRepository $repository
     * @return JsonResponse
     */
    public function list(CategoryRepository $repository): JsonResponse
    {
        $categories = $repository->findAll();

        return $this->response($categories);
    }

    /**
     * @Route("/category/{uuid}", name="api_category_show", requirements={"uuid"="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}"})
     * @Method("GET")
     * @ParamConverter(class="App:Category", converter="uuid_converter", name="category")
     * @param Category $category
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function show(?Category $category): JsonResponse
    {
        if (!$category) {
            throw $this->createNotFoundException();
        }
        return $this->response($category);
    }

    /**
     * @Route("/category", name="api_category_new")
     * @Method("POST")
     * @param Request $request
     * @param CategoryManager $manager
     * @return JsonResponse
     */
    public function new(Request $request, CategoryManager $manager): JsonResponse
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category,['category_id' => $category->getId()]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
//            $category = $form->getData();

            $manager->saveCategory($category);

            return $this->response($category,['category-create'], 200);
        }

        return $this->errors($form);
    }

    /**
     * @Route("/category/{uuid}", name="api_category_update", requirements={"uuid"="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}"})
     * @Method("PUT")
     * @ParamConverter(class="App:Category", converter="uuid_converter", name="category")
     * @param Category $category
     * @param Request $request
     * @param CategoryManager $manager
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function update(?Category $category, Request $request, CategoryManager $manager): JsonResponse
    {
        if (!$category) {
            throw $this->createNotFoundException();
        }
        $form = $this->createForm(CategoryType::class, $category, ['category_id' => $category->getId()]);
        $form->submit($request->request->all());

        if($form->isValid()) {
            $category = $form->getData();
            $manager->saveCategory($category);

            return $this->response($category);
        }

        return $this->errors($form);
    }

    /**
     * @Route("/category/{uuid}", name="api_category_delete", requirements={"id"="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[4][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}"})
     * @Method("DELETE")
     * @ParamConverter(class="App:Category", converter="uuid_converter", name="category")
     * @param Category|null $category
     * @param CategoryManager $manager
     * @return JsonResponse
     * @throws NotFoundHttpException | LogicException
     */
    public function delete(?Category $category, CategoryManager $manager): JsonResponse
    {
        if (!$category) {
            throw $this->createNotFoundException();
        }

        if ($category->hasChildren()) {
            throw new LogicException('error.category.has.children');
        }

        $manager->removeCategory($category);

        return $this->noContent();
    }
}
