<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Role;
use App\Form\RoleType;
use App\Manager\Role\RoleManager;
use App\Repository\RoleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class RoleController extends ApiController
{
    /**
     * List all roles
     *
     * @Route("/role", name="api_role_list")
     * @Method({"GET"})
     * @param RoleRepository $repository
     * @return JsonResponse
     */
    public function list(RoleRepository $repository): JsonResponse
    {
        $roles = $repository->findAll();

        return $this->response($roles);
    }

    /**
     * Get role by id
     *
     * @Route("/role/{uuid}", name="api_role_show")
     * @ParamConverter(class="App:Role", converter="uuid_converter", name="role")
     * @Method({"GET"})
     * @param Role|null $role
     * @return JsonResponse
     */
    public function show(?Role $role): JsonResponse
    {
        if (!$role) {
            throw $this->createNotFoundException();
        }

        return $this->response($role);
    }

    /**
     * Add new role
     *
     * @Route("/role", name="api_role_new")
     * @Method({"POST"})
     * @param Request $request
     * @param RoleManager $manager
     * @return JsonResponse
     */
    public function new(Request $request, RoleManager $manager): JsonResponse
    {
        $form = $this->createForm(RoleType::class);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $role = $form->getData();
            $manager->saveRole($role);

            return $this->response($role);
        }

        return $this->errors($form);
    }

    /**
     * Update role
     *
     * @Route("/role/{uuid}", name="api_role_update")
     * @ParamConverter(class="App:Role", converter="uuid_converter", name="role")
     * @Method({"PUT"})
     * @param Request $request
     * @param Role|null $role
     * @param RoleManager $manager
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function update(?Role $role , Request $request, RoleManager $manager): JsonResponse
    {
        if (!$role) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(RoleType::class, $role);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $role = $form->getData();
            $manager->saveRole($role);

            return $this->response($role);
        }

        return $this->errors($form);
    }

    /**
     * Delete role by id
     *
     * @Route("/role/{uuid}", name="api_role_delete")
     * @ParamConverter(class="App:Role", converter="uuid_converter", name="role")
     * @Method({"DELETE"})
     * @param RoleManager $manager
     * @param Role|null $role
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function delete(?Role $role, RoleManager $manager)
    {
        if (!$role) {
            throw $this->createNotFoundException();
        }

        $manager->removeRole($role);

        return $this->noContent();
    }
}
