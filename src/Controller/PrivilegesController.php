<?php declare(strict_types=1);

namespace App\Controller;

use App\Service\PrivilegesService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class PrivilegesController extends ApiController
{

    /**
     * Privileges list in two formats: flat | nested
     *
     * @Route("/privilege", name="api_privileges_list")
     * @Method({"GET"})
     * @Rest\QueryParam(name="nested", requirements="(true|false)", default="false")
     * @param ParamFetcher $paramFetcher
     * @return JsonResponse
     */
    public function list(ParamFetcher $paramFetcher): JsonResponse
    {
        $nested = $paramFetcher->get('nested') == 'true';

        return $this->response(PrivilegesService::list($nested));
    }
}