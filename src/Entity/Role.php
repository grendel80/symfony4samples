<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table("ls_role")
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
    * @var Uuid
    * @ORM\Id
    * @ORM\Column(type="uuid", unique=true)
    * @ORM\GeneratedValue(strategy="CUSTOM")
    * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="json")
     */
    protected $privileges;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $custom = false;

    public function __construct()
    {
        $this->privileges = [];
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName(string $name): Role
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|array
     */
    public function getPrivileges(): ?array
    {
        return $this->privileges;
    }

    /**
     * @param array $privileges
     * @return Role
     */
    public function setPrivileges(array $privileges): Role
    {
        $this->privileges = array_values($privileges);
        return $this;
    }

    /**
     * @return mixed
     */
    public function isCustom(): bool
    {
        return $this->custom;
    }

    /**
     * @param mixed $isCustom
     * @return Role
     */
    public function setCustom(bool $isCustom): Role
    {
        $this->custom = $isCustom;
        return $this;
    }

    /**
     * @param string $privilege
     * @return Role
     */
    public function addPrivilege(string $privilege): Role
    {
        if (!in_array($privilege, $this->privileges)) {
            $this->privileges[] = $privilege;
        }
        $this->privileges = array_values($this->privileges);
        return $this;
    }

    /**
     * @param string $privilege
     * @return Role
     */
    public function removePrivilege(string $privilege): Role
    {
        $result = array_search($privilege, $this->privileges);
        if ($result !== false) {
            unset($this->privileges[$result]);
        }
        $this->privileges = array_values($this->privileges);
        return $this;
    }
}
